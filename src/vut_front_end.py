#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
module_name: vut_front_end
---------------------------------------
Author: Rodrigo Peixoto
Data: 10/02/2008
---------------------------------------
Description:
 - This module runs all the gerenation 
 process.
=======================================
License: GPL
======================================================================

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================
"""
from vut_checker import *
from vut_generator import *
from vut_parser import *
import sys
import psyco
psyco.full()

ARGS_CAN_BE = ['skeleton','makefile']

def verify_parameters(params):
    l = len(params)
    executable_name = params[0]
    params = params[1:]
    ret = {}
    aux = []
    for param in params:
        aux = param.split('=')
        ret[aux[0]]= aux[1].split(',')

    aux = None
    return ret

class InvalidArgumentException(Exception):
    pass

def run_vut_generator(args=[]):
    #arg = "example5.vut"
    #args = sys.argv
    flags = verify_parameters(args)
    #print flags
    lflags = len(flags)
    gen_param = []
    try:
        try:
            file_name = flags['vut_file'][0]
        except KeyError:
            raise InvalidArgumentException()

        try:
            gen_param = flags['gen']
            assert set(gen_param).issubset(set(ARGS_CAN_BE)), "|VUT ERROR| > The gen's values are wrong!" 
        except KeyError:
            if lflags == 2: raise InvalidArgumentException()

#        if len(args) == 3:
#            if args[1] in ARGS_CAN_BE:
#                gen_param = args[1]
#                file_name = args[2]
#            else:
#                raise InvalidArgumentException()

#        elif (len(args) == 2):
#            file_name = args[1]
#        else:
#            raise InvalidArgumentException()

        print "#################"
        print "# @VUTGenerator #"
        print "#################"
        print "Phases: Check -> Parse -> Code Generation"
        print "\nPHASE 1 [Check] --------------------------------------------\n"
        VUTChercker(file_name).check_all()
        print "\nPHASE 2 [Parse] --------------------------------------------\n"
        vm = VUTParser(file_name).parse_all()
        print "\nPHASE 3 [Code Generation] --------------------------------------------\n"
        VUTGenerator(vm,gen_param).gen()
        #VUTGenerator(vm).gen()
        print u"\n\nGeneration complete @VUTGenerator."

    except InvalidArgumentException:
        print "|VUT ERROR| > The arguments are not valid. Please, check it out."
        print "|VUT INFO| > Usage: ./vutg gen=<item1>,<item2> vut_file=<file_name.vut>"

    except KeyboardInterrupt:
        print u"\n\nGeneration interrupted! @VUTGenerator."

if __name__=="__main__":
    run_vut_generator("vutg gen=skeleton,makefile vu_file=example5.vut")

