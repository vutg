#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
module_name: vut_checker
---------------------------------------
Author: Rodrigo Peixoto
Data: 10/02/2008
---------------------------------------
Description:
 - This moludes checks the integrity of
 the vut file, based in the basics
 rules.
=======================================
License: GPL
======================================================================

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================
"""

from elementtree.ElementTree import *
from utils import StopGeneration
import re
import sys
import psyco
psyco.full()
class InvalidTagException(Exception):
    pass


class VUTChercker:

    def __init__(self, xml_source):
        try:
            print "Openning the vut file..."
            self.vut = ElementTree(file=xml_source)
        except IOError:
            print "|VUT Error| > The file %s is not exists!" % xml_source
            raise StopGeneration()
        self.root = self.vut.getroot()
        mod = self.root.get("module_name")
        #assert mod, """Module name not defined in vut tag. Usage '<vut module_name="name">...</vut>'"""
        if not mod:
            print """Error: Module name not defined in vut tag.\nUsage '<vut module_name="name">...</vut>'"""
            raise StopGeneration()
        #assert re.match(r'^[a-zA-z][a-zA-Z0-9_]*$', mod), "The module_name can't to start with number or to contain a special character!" #Verifica se deu match, retorna um obj sre se sim e None se nￃﾃￂﾣo
        if not re.match(r'^[a-zA-z][a-zA-Z0-9_]*$', mod):
            print "Error: invalid module_name, it can't to start with number or to contain a special character!" #Verifica se deu match, retorna um obj sre se sim e None se nￃﾃￂﾣo
            raise StopGeneration()
        print "Init checking %s..." % xml_source

    def clear_comments(self, dlist):
        for i in dlist:# removing commets
            if "#" in i:
                dlist[dlist.index(i)] = i[:i.index("#")]
        return map(str.strip, dlist)


    def check_waveform(self, *args):
            print "checking waveform tag..."
            waves = map(str.strip, args[0].text.splitlines())
            waves = [x for x in waves if((x!= "") and ("--" not in x) and ("=" not in x))]
            #print waves
            for signal in waves:
                regexp = re.compile(r"""
                                        (^[a-zA-z])\w*        #signal name
                                        (\s*\[\d+\:\d+\])?\s* #bit slice
                                        (
                                            i\@\|\s*             #input definition
                                                ((
                                                    [0-9A-Fa-f ]+|   #hexa values
                                                    R\d+|            #random value definition
                                                    random_alloc\(\[(R\d+)(\,R\d+)*\]\,\d+\)| # teste
                                                    include_memory\(\"\w*\.mem\"\)| #random randomic values allocation
                                                )\|)* |
                                            o\@\|\s*             #output definition
                                                ((
                                                    [0-9A-Fa-f ]+|   #hexa values
                                                    ref_function\(([a-zA-Z]\w*)(\,[a-zA-Z]\w*)*\)+| #reference function definition
                                                    include_memory\(\"\w*\.mem\"\)| #random randomic values allocation
                                                )\|)*
                                        )$
                                    """,re.VERBOSE)

                #assert regexp.match(signal),"Construction error in the signal: %s" % signal
                if not regexp.match(signal):
                    print "Error: Construction error in the signal: %s" % signal
                    raise StopGeneration()
                #[a-zA-Z0-9_] = \w

            waves = map(lambda a : a.replace(" ", ""),waves)

    def check_gen_with(self, *args):
        '''

        @param *args:
        '''
        print "checking gen_with tag...\n    checking randrange values..."
        ran = map(str.strip,args[0].text.splitlines())
        ran = [x for x in ran if((x!= "") and (x[0]!="#"))]
        ran = self.clear_comments(ran)

        for i in ran:
            assert re.match(r'R\d+\s*\=\s*\([0-9A-Fa-f]+\,[0-9A-Fa-f]+\)',i), "Invalid Attribution in %s" % i

        for code in args[0].getchildren():
            var = code.get("output")
            if var:
                if not re.match(r'[a-zA-Z][a-zA-Z0-9_]*$',var):
                    print "Error: Invalid output name into python_code tag."
                    raise StopGeneration()
            else:
                print """Error: Output not defined into python_code tag.\nUsage <python_code output="port_name">...code...</python_code>!"""
                raise StopGeneration()
            self.python_code_parser(code.text)
        #print ran

    def check_time_scale(self, *args):
        '''

        @param *args:
        '''

        print "checking time_scale tag..."
        t_div = args[0].get("t_div")
        unit = args[0].get("unit")
        if not t_div:
            print  """Error: Time division not defined in time_scale tag.\n
Usage <time_scale t_div='val_integer' unit="unit{(ump)s}"/>!"""
            raise StopGeneration()
        if not unit:
            print "Error: Unit not defined in time_scale tag!"
            raise StopGeneration()

    def check_all(self):
        '''
        '''
        root_children = self.root.getchildren()
        switch = {"waveform": self.check_waveform,
                  "gen_with": self.check_gen_with,
                  "time_scale": self.check_time_scale}

        for item in root_children:
            try:
                switch[item.tag](item)
            except KeyError, e:
                raise InvalidTagException(e)
        print "Check complete successful!!!"

    def python_code_parser(self, pycode):
        #TODO: Ajsutar isso aqui!!!
        print "    checking python code tag..."
        if not pycode:
            print "Error: The python_code tag is empty."
            raise StopGeneration()
        code = self.clear_comments(pycode.splitlines())
        code = [x for x in code if(x!= "")]

        try:
            if re.match(r"\s*include_python_file\(\"(((\/\w+)+|\.)\/)?\w+.py[oc]?\"\)\s*$",pycode):
                print "        checking external reference function's call...ok"# % pycode.strip()

            elif re.match(r'(?!include_python_file)',pycode):
                print "        checking local reference function's call...",
                exec("\n".join(code))
                print "ok"
            else: raise Exception("VUT Syntax error!")
        except:
            print "Python reference model code error: ", sys.exc_info()

if __name__ == '__main__':
    test = VUTChercker("example5.vut")#sys.argv[1])
    test.check_all()
