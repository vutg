#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
module_name: utils
---------------------------------------
Author: Rodrigo Peixoto
Data: 11/02/2008
---------------------------------------
Description:
 - This moludes contains the basics
 tools.
=======================================
License: GPL
======================================================================

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================
"""
from random import randrange
import re
import sys
import threading
import psyco
psyco.full()

SYSTEM_DELAY=5

class StopGeneration(KeyboardInterrupt):
    pass

def threaded(func):
    def proxy(*args,**kwargs):
        thread = Thread(target=func,args=args,kwargs=kwargs)
        thread.start()
        return thread
    return proxy

def turbo(func):
    return psyco.proxy(func)

class VerilogModule(object):
    def __init__(self, module_name=""):
        self.name=module_name #It must respect the regexp [a-zA-Z]\w*
        self.in_ports=[] #Can be a bit (a) or vector ex.: (a,(7,0))
        self.out_ports=[] #Can be a bit (a) or vector ex.: (a,(7,0))
        self.time_slice=() #Must be ((division, unit) ,(precision, unit))
        self.in_ports_behavior=[]
        self.out_ports_behavior=[]
        self.out_ports_signal_dependecies = {}
        self.ran_range = {}
        self.ref_functions = {}
        self.random_alloc = {} # 'sig_name': ([R1,R2,R3,...,R2], n)signal name list that contains RA

    def set_in_ports_behavior(self, behav):
        self.in_ports_behavior=behav

    def get_port_behav(self, name):
        ret = None
        for i in self.in_ports_behavior+self.out_ports_behavior:
            if i.name == name:
                ret = i
                break
        return ret

    def set_out_ports_behavior(self, behav):
        self.out_ports_behavior=behav

    def alloc_random_values():
        pass

    def get_first_Rn(self,l):
        ret = -1
        for i in l:
            ret+=1
            if re.match(r"R\d+",i):
                break
        return ret
    @turbo
    def gen_random_values(self):
        print "Generating the random values, it can take much time..."

        regexp = re.compile("R\d+")
        cleaning = lambda x: re.sub(r"L","",hex(x)[2:])

        for beh in self.in_ports_behavior:
            tmpl = []
            for val in beh.content:
                if regexp.match(val):
                    try:
                        ri,ro = self.ran_range[val]
                        #TODO: Verificar o [2:-1] da linha abaixo
                        #beh.content[beh.content.index(val)] = hex(randrange(ri,ro))[2:].replace("L","")
                        tmpl.append(randrange(ri,ro))
                    except KeyError:
                        #print sys.exc_info()
                        print "!Error: The random range to %s value is not defined in vut file!" % val
                        sys.exit(1)
                    except ValueError:
                        print "!Error: Check the random range to %s value it is wrong!" % val
                        sys.exit(1)
            beh.content = beh.content[:self.get_first_Rn(beh.content)]
            beh.content+=map(cleaning,tmpl)
            #print beh.content[:10]
        #DEBUG: print self.in_ports_behavior

    @turbo
    def resolve_reference_functions(self):
        #TODO: Otimizar esse trecho de código, por causa dele temos vários picos de memória enquanto executa-se o reference model.
        print "Applying reference model defined..."

        for kref in self.ref_functions:
            params = self.out_ports_signal_dependecies[kref]
            reff = self.ref_functions[kref]

#            mem = []
#            for i in params:
#                mem.append(self.get_port_behav(i))
#            tmp = []
#            for x in mem:
#                tmp.append([int(m,16) for m in x.content])
#            aux = []
#            exec "aux = zip(%s)" % (",".join(tmp))
#            res = None
#            out = []
#            for y in aux:
#                exec "res = reff%s" % str(y)
#                out.append(res)
            #print tmp
            try:
                test = {}
                for i in params:
                    test[i] = [int(x,16) for x in self.get_port_behav(i).content]
                out = reff(test)
                result = [hex(x)[2:].replace("L","") for x in out]

                tmp = self.out_ports_behavior[self.out_ports_behavior.index(self.get_port_behav(kref))].content
                tmp.pop()
                if len(tmp):
                    tmp+=result[(len(tmp)):]
                else:
                    tmp+=result
            except AttributeError:
                print "|VUT ERROR| > The reference function's dependencies should be wrong, please fix."
                raise StopGeneration()


    def __repr__(self):
        return "============================\nModule: %s\n----------------------------\nIn_ports: %s\
\nOut_ports: %s\n============================" % (self.name, self.in_ports, self.out_ports)


class MemoryIO(object):
    def __init__(self, pname="", pcontent=[]):
        self.name = pname
        self.content = pcontent

    def get_content(self):
        return self.content

    def __repr__(self):
        return "Memory name: %s || Content: %s\n" % (self.name, self.content)


BASIC_SKELETON=\
"""
/*
@The skeleton was generated by VUTGenerator@
=======================================
module_name: %(module_name)s
---------------------------------------
Author: <author>
Data: <date>
---------------------------------------
Description: <description>
=======================================
*/

module %(module_name)s(%(ports)s);
//Inputs
%(inputs)s

//Outputs
%(outputs)s

//Wires
%(wires)s

//Regs
%(regs)s

//Behavior

%(behavior)s

endmodule
"""



BASIC_VUT_BEHAVIOR=\
"""
%(memories)s
%(tmpmem)s
    integer k;
    event send, ready;

%(module)s

    initial $display("|VUT INFO| > Load memories... ");

%(initialmems)s

    initial begin
        $dumpfile ("waveform.vcd");
        $dumpvars;
        $display("|VUT INFO| > Init Simulation.");
        #1;
%(initregs)s
        k = -1;
        #4 -> ready;
    end


    always @ ready begin
        k = k + 1;
%(mematt)s
        if (k >= %(memlen)s) begin
            $display("|VUT OK| > All the signals are right-right!\\n\\n By Rodrigo Peixoto\\n");
            #5 $finish;
        end //if
        else #%(delay)s -> send;
    end


    always @ send begin
%(iferrors)s
        #3 -> ready;
    end

"""

BASIC_IF=\
"""        if (%(port)s !== from_mem_%(port)s) begin
            $display("|VUT FAIL| > Error in %(port)s value at time %%0dns!!!",$time);
            $finish;
        end //if"""

MAKEFILE=\
"""all:
\tcver %(module_name)s.v vut_%(module_name)s.v
  
clean:
\trm vut_%(module_name)s.v *.mem waveform.vcd

"""
