#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
module_name: vut_analyser
---------------------------------------
Author: Rodrigo Peixoto
Data: 10/02/2008
---------------------------------------
Description:
 - This moludes fetchs the data from
 the waveform to generate the Module
 Skeleton and the UnitTest Module
=======================================
License: GPL
======================================================================

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================
"""

from elementtree.ElementTree import *
from vut_generator import *
from utils import *
import re
import sys
import random
import psyco
psyco.full()

class VUTParser:

    def __init__(self, xml_source):
        self.vut = ElementTree(file=xml_source)
        self.root = self.vut.getroot()
        self.vmodule = VerilogModule(self.root.get("module_name"))
        print "Initing parser to %s..." % xml_source

    def clear_comments(self, dlist):
        for i in dlist:# removing commets
            if "#" in i:
                dlist[dlist.index(i)] = i[:i.index("#")]
        return map(str.strip, dlist)


    def parse_waveform(self, *args):
        print "parsing waveform tag..."
        waves = map(str.strip, args[0].text.splitlines())
        waves = [x for x in waves if((x!= "") and ("-" not in x) and ("=" not in x))]
        for signal in waves:
            t = re.findall(r'(^[a-zA-z]\w*( )*\[)|(^[a-zA-z]\w*( )*[io]\@)', signal)[0]
            sig_name = [x for x in t if x!=""][0].split()[0].replace('[', '') #Cleaning all once!
            slice = re.findall(r'\[\d+\:\d+\]', signal)
            port = None
            if slice:
                port = (sig_name, tuple(map(int, re.findall(r'\d+', slice[0]))))
            else: port = (sig_name,)

            if re.match(r'.*i@.*', signal):
                self.vmodule.in_ports.append(port)
                ref = re.findall(r"random_alloc\(.*\)",signal)
                if ref:
                    print "    found random_alloc to %s: %s" % (sig_name,ref[0])
                    rs = re.findall(r"R\d+",ref[0])
                    n = int(re.findall(r"\,\d+",ref[0])[0][1:])
                    self.vmodule.random_alloc[sig_name] = (rs,n)
                    #DEBUG: print self.vmodule.random_alloc


            else:
                self.vmodule.out_ports.append(port)
                ref = re.findall(r"ref_function\(.*\)",signal)
                sigs = []
                if ref:
                    print "    found reference_function to %s: %s" % (sig_name, ref[0])
                    sigs = re.findall(r"\(.*\)",ref[0])[0][1:-1].split(',')
                    self.vmodule.out_ports_signal_dependecies[sig_name] = sigs

        #DEBUG: print "What HELL!!!", self.vmodule.out_ports_signal_dependecies
        waves = map(lambda a : a.replace(" ", ""), waves)
        self.parse_signal_values(waves)

    def parse_gen_with(self, *args):
        '''
        @param *args:
        '''
        print "parsing gen_with tag...\n    parsing randrange values..."
        ran = map(str.strip, args[0].text.splitlines())
        ran = [x for x in ran if((x!= "") and (x[0]!="#"))]
        #ran = self.clear_comments(ran)
        for i in ran:
            self.vmodule.ran_range[re.findall(r'R\d+', i)[0]] =\
             (int(re.findall(r'[0-9A-Fa-f]+\s*\,', i)[0][:-1],16),\
              int(re.findall(r'[0-9A-Fa-f]+\s*\)', i)[0][:-1],16))
        #print self.vmodule.ran_range
        for code in args[0].getchildren():
            var = code.get("output")
            assert var, "Output not defined into python_code tag.\n"\
                        'Usage <python_code output="port_name">...code...</python_code>!'
            port_name = re.findall(r'[a-zA-Z][a-zA-Z0-9_]*$', var)[0]
            assert port_name, ""
            self.python_code_parser(port_name, code.text)

    def parse_time_scale(self, *args):
        '''
        @param *args:
        '''
        print "parsing time_scale tag..."
        t_div = args[0].get("t_div")
        unit = args[0].get("unit")
        assert t_div, 'Time division not defined in time_scale tag.\n'\
                      'Usage <time_scale t_div="val_integer" unit="unit{(ump)s}"/>!'
        assert unit, "Unit not defined in time_scale tag!"

    def parse_signal_values(self, pwaves):
        print "    creating memories..."
        list_imem = []
        list_omem = []
        for wav in pwaves:
            sig_name = re.sub("\[\d+:\d+\]", '', wav[:wav.index('@')-1])
            #Random alloc values
            if sig_name in self.vmodule.random_alloc:
                ran_val = []
                tmp_val,n = self.vmodule.random_alloc[sig_name]
                for i in range(n):
                    ran_val.append(tmp_val[random.randrange(len(tmp_val))])
                wav = re.sub(r"random_alloc\(.*\)","|".join(ran_val),wav)
                #DEBUG: print "TESTE DE RANDOMALLOC",wav

            values = wav[wav.index('@')+1:].split('|')[1:-1]
            if wav[wav.index('@')-1] == 'i':
                mem = MemoryIO("%s"% sig_name, values)
                #print mem
                #exec ("imem_%s = %s" % (sig_name, values)) #wav[wav.index('@')+1:].split('|')[1:-1]))
                list_imem.append(mem)
            else:
                mem = MemoryIO("%s"% sig_name, values)
                #exec ("omem_%s = %s"% (sig_name, values)) #wav[wav.index('@')+1:].split('|')[1:-1]))
                #exec("list_omem.append(omem_%s)"% sig_name)
                list_omem.append(mem)
        #print list_imem
        #print list_omem
        self.vmodule.set_in_ports_behavior(list_imem)
        self.vmodule.set_out_ports_behavior(list_omem)


    def parse_all(self):
        '''
        '''
        root_children = self.root.getchildren()
        switch = {"waveform": self.parse_waveform,
                  "gen_with": self.parse_gen_with,
                  "time_scale": self.parse_time_scale}

        for item in root_children:
            try:
                switch[item.tag](item)
            except KeyError, e:
                raise InvalidTagException(e)
        print "Parse complete successful!!!"
        return self.vmodule

    def python_code_parser(self, port_name, pycode):
        #TODO: Ajsutar isso aqui!!!
        print "    parsing python code tag..."

        code = self.clear_comments(pycode.splitlines())
        code = [x for x in code if(x!= "")]

        try:
            ref_function = None
            if re.match(r"\s*include_python_file\(\"(((\/\w+)+|\.)\/)?\w+.py[oc]?\"\)\s*$",pycode):
                #print "        " + pycode.strip()
                print "        fetching external reference function... %s" % pycode.strip()
                function_file = re.findall(r"\"(((\/\w+)+|\.)\/)?\w+.py[oc]?\"", pycode)[0]
                try:
                    exec """import %(file)s;ref_function = %(file)s.reference_function""" % {'file':port_name}
                except ImportError:
                    print "Error: Python reference model error: ", sys.exc_info()[1]
                    raise StopGeneration()
            else:
                exec("ref_function = " + "\n".join(code))
                print "        fetching local reference function... ", "\n\t".join(code)
                #DEBUG: print "test ref_function: ", ref_function(10,20)
            self.vmodule.ref_functions[port_name] = ref_function
            #DEBUG: print self.vmodule.ref_functions
        except SyntaxError:
            print "Python reference model code error: ", sys.exc_info()[1]
            raise StopGeneration()
if __name__ == '__main__':
    test = VUTParser("example5.vut")
    vm = test.parse_all()
    #VUTGenerator(vm).gen()


