#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
=======================================
module_name: vut_generator
---------------------------------------
Author: Rodrigo Peixoto
Data: 11/02/2008
---------------------------------------
Description:
 - This modules generates the Verilog
 module skeleton, based in the vut
 file.
=======================================
License: GPL
======================================================================

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

=======================================================================
"""

from utils import *
import re
import os
import psyco
psyco.full()
DEST_GEN_FILES="./gen/"

class VUTGenerator:
    def __init__(self, vmodule, flags):
        self.flags=flags
        self.vmodule = vmodule
        self.vmodule.gen_random_values()
        self.vmodule.resolve_reference_functions()
        print "Code generation stars"
        try:
            os.mkdir("gen")
        except OSError:
            pass

    def get_(self, items=[], string_label=""):
        for sig in items:
            if len(sig) > 1:
                yield "    %s [%d:%d] %s;" % (string_label, sig[1][0], sig[1][1], sig[0])
            else:
                yield "    %s %s;" % (string_label, sig[0])

    def get_memories(self):
        items = self.vmodule.in_ports + self.vmodule.out_ports
        verf_items = self.vmodule.in_ports_behavior + self.vmodule.out_ports_behavior
        #print items
        #print verf_items
        for sig in items:
            mem_len = len(verf_items[items.index(sig)].get_content()) -1
            if len(sig) > 1:
                yield "    reg [%d:%d] mem_%s [0:%d];" % (sig[1][0], sig[1][1], sig[0], mem_len)
            else:
                yield "    reg mem_%s [0:%d];" % (sig[0], mem_len)

    def gen_module_skeleton(self):
        inputs = list(self.get_(self.vmodule.in_ports, "input"))
        inputs.sort()
        outputs = list(self.get_(self.vmodule.out_ports, "output"))
        outputs.sort()
        wires = list(self.get_(self.vmodule.in_ports, "wire"))
        wires.sort()
        regs = list(self.get_(self.vmodule.out_ports, "reg"))
        regs.sort()
        verilog_code = BASIC_SKELETON % {'module_name':self.vmodule.name,
                                'ports':','.join([x[0] for x in self.vmodule.in_ports] + [x[0] for x in self.vmodule.out_ports]),
                                'inputs':"\n".join(inputs),
                                'outputs':"\n".join(outputs),
                                'wires':"\n".join(wires),
                                'regs':"\n".join(regs),
                                'behavior': ""}
        file = open(DEST_GEN_FILES + "%s.v" % self.vmodule.name, "w")
        file.writelines(verilog_code)
        file.close()

    def gen_makefile(self):
        makefile_code = MAKEFILE % {'module_name': self.vmodule.name}
        file = open(DEST_GEN_FILES + "makefile","w")
        file.write(makefile_code)
        file.close()

    def gen_vut(self):

        regs = list(self.get_(self.vmodule.in_ports, "reg"))
        regs.sort()
        wires = list(self.get_(self.vmodule.out_ports, "wire"))
        wires.sort()
        memos = list(self.get_memories())
        memos.sort()

        def gen_module_inst():
            ret="    %(module)s test(\n%(cons)s);"
            cons = []
            for p in self.vmodule.in_ports + self.vmodule.out_ports:
                cons.append("        .%(port)s(%(port)s)" % {'port':p[0]})
            return ret % {'module':self.vmodule.name,
                          'cons':',\n'.join(cons)}
        def gen_tmp_mem():
            ret=[]

            for p in self.vmodule.out_ports:
                 if len(p) > 1:
                     ret.append("    reg [%d:%d] from_mem_%s;" % (p[1][0], p[1][1], p[0]))
                 else:
                     ret.append("    reg from_mem_%s;" % p[0])
            return '\n'.join(ret)

        def gen_initial_mems():
            ret = []
            for p in self.vmodule.in_ports:
                ret.append('    initial $readmemh("%(port)s.mem",mem_%(port)s);' % {'port':p[0]})
            for p in self.vmodule.out_ports:
                ret.append('    initial $readmemh("%(port)s.mem",mem_%(port)s);' % {'port':p[0]})
            return '\n'.join(ret)

        def gen_init_regs():
            ret = []
            for p in self.vmodule.in_ports:
                ret.append("        %s = 0;" % p[0])
            return '\n'.join(ret)

        def gen_set_regs():
            ret = []
            for p in self.vmodule.in_ports:
                ret.append(8*" " + "%(p)s = mem_%(p)s[k];" % {'p':p[0]})
            for p in self.vmodule.out_ports:
                ret.append(8*" " + "from_mem_%(p)s = mem_%(p)s[k];" % {'p':p[0]})
            return '\n'.join(ret)

        def gen_sens_list():
            ret = []
            for p in self.vmodule.out_ports:
                ret.append(p[0])
            return ' or '.join(ret)

        def gen_if_errors():
            ret = []
            for p in self.vmodule.out_ports:
                ret.append(BASIC_IF % {'port': p[0]})
            return '\n'.join(ret)

        behavior_code = BASIC_VUT_BEHAVIOR % {'memories':'\n'.join(memos),
                                              'tmpmem': gen_tmp_mem(),
                                              'module': gen_module_inst(),
                                              'initialmems': gen_initial_mems(),
                                              'initregs': gen_init_regs(),
                                              'memlen': len(self.vmodule.in_ports_behavior[0].content),
                                              'delay': 2,
                                              'mematt': gen_set_regs(),
                                              'outlist':gen_sens_list(),
                                              'iferrors': gen_if_errors(),
                                              'tofin': len(self.vmodule.in_ports_behavior[0].content) * 10
                                              }

        verilog_code = BASIC_SKELETON % {'module_name':"vut_" + self.vmodule.name,
                                'ports': "",
                                'inputs':"",
                                'outputs':"",
                                'wires':"\n".join(wires),
                                'regs':"\n".join(regs),
                                'behavior': behavior_code}


        file = open(DEST_GEN_FILES + "vut_%s.v" % self.vmodule.name, "w")
        file.writelines(verilog_code)
        file.close()

        for in_p in self.vmodule.in_ports_behavior:
            f = open(DEST_GEN_FILES + in_p.name + ".mem", "w")

            f.write("\n".join(in_p.content) + '\n')
            f.close()
#            self.gen_random_values(in_p)

        for out_p in self.vmodule.out_ports_behavior:
            f = open(DEST_GEN_FILES + out_p.name + ".mem", "w")
            f.write("\n".join(out_p.content) + '\n')
            f.close()

#    def gen_random_values(self, cont):
#        regexp = re.compile("R\d+")
#        for val in cont:
#            if regexp.match(val):
#                regexp.findall(val)
#                cont[cont.index(val)] = hex(randrange(0,255))[2:]
#                print cont
#
    def gen(self):
        if 'skeleton' in self.flags:
            if os.path.exists("gen/%s.v" % self.vmodule.name):
                verif = re.compile(r"[ynYN]$")
                res = raw_input("|VUT QUESTION| > Already exists a %s.v in gen folder, do you want to override it?[y/n]" % self.vmodule.name)
                while not verif.match(res):
                    res = raw_input("|VUT QUESTION| > Already exists a %s.v in gen folder, do you want to override it?[y/n]" % self.vmodule.name)
                if re.match(r"[yY]$",res):
                    try:
                        os.system("cp gen/%s.v ./%s.v~" % (self.vmodule.name,self.vmodule.name))
                        print "|VUT INFO| > A backup file will be generate in %s" % os.getcwd()
                    except:
                        import sys
                        print sys.exc_info() 
                    self.gen_module_skeleton()
                    print "--> skeleton's code generated %s.v" % self.vmodule.name
            else:
                self.gen_module_skeleton()
                print "--> skeleton's code generated %s.v" % self.vmodule.name
        if 'makefile' in self.flags:
            self.gen_makefile()
            print "--> makefile generated in %s" % DEST_GEN_FILES
        self.gen_vut()
        print "--> unittest's code generated vut_%s.v" % self.vmodule.name

if __name__ == '__main__':
    vm = VerilogModule("Teste_de_geracao")
    vm.in_ports.append(('a', (3, 0)))
    vm.in_ports.append(('c',))
    vm.in_ports.append(('d', (3, 0)))
    vm.in_ports_behavior.append( MemoryIO('a',['R1','F','R2','9','4','R3','R2','R1','R2','R3','R4']))
    vm.in_ports_behavior.append( MemoryIO('c',['4','R3','A','R4','R1','R2','R3','R4','R3','R2','R1']))
    vm.in_ports_behavior.append( MemoryIO('d',['4','F','A','9']))
    vm.out_ports.append(('e', (3, 0)))
    vm.out_ports.append(('f', (3, 0)))
    vm.out_ports.append(('g', (3, 0)))
    vm.out_ports_behavior.append( MemoryIO('e',['4','F','A','9']))
    vm.out_ports_behavior.append( MemoryIO('f',['4','F','A','9','c','6']))
    vm.out_ports_behavior.append( MemoryIO('g',['4','F','A','9']))
    vm.ran_range['R1'] = (0,24)
    vm.ran_range['R2'] = (25,49)
    vm.ran_range['R3'] = (50,74)
    vm.ran_range['R4'] = (75,100)
    vm.ref_functions['e'] = "lambda a,c:a+c"
    vm.out_ports_dep['e'] = ['a','c']
    test = VUTGenerator(vm)
    test.gen()
