
def reference_function(tmp):
    aux = zip(tmp['a'],tmp['b'])
    def overflow(t=()):#t == (a,b); sum(t) == a+b
        ret = 0
        if sum(t) > 255:
            ret = 1;
        return ret
    return map(overflow,aux)
