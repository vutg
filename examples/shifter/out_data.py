# -*- coding: utf-8 -*-
"""
clk      i@|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|
------------------------------------------------------------------------
in_data  i@|random_alloc([R1,R2,R3],15)|
------------------------------------------------
n        i@|random_alloc([R4],30)|
"""
def max_lenght(signals):
    ret =  min(map(len,signals.itervalues()))
    print ret
    return ret

def reference_function(signals):
    ret = []
    prev_clk = 0
    for step in range(max_lenght(signals)):

        clk = signals['clk'][step]
        reset = signals['reset'][step]
        enable =  signals['enable'][step]
        in_data = signals['in_data'][step]
        n = signals['n'][step]

        if clk and (clk != prev_clk):
            if reset:
                ret.append(0)

            elif enable:
                ret.append( in_data >> n)

            else: ret.append(ret[-1])

        else:
            try:
                ret.append(ret[-1]) 
            except:
                ret.append(0)
        prev_clk = clk

    return ret


