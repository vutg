# -*- coding: utf-8 -*-
"""
clk      i@|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|
------------------------------------------------------------------------
in_data  i@|random_alloc([R1,R2,R3],15)|
------------------------------------------------
clear    i@|random_alloc([R4],30)|
------------------------------------------------
load     i@|random_alloc([R4],30)|
"""
def min_lenght(signals):
    ret =  min(map(len, signals.itervalues()))
    print ret
    return ret

def fetch_signals(signals, step):
    return (signals['clear'][step], signals['clk'][step], signals['load'][step], signals['in_data'][step])

def reference_function(signals):

    ret = []
    prev_clk = 0
    for step in range(min_lenght(signals)):
        clear, clk, load, in_data = fetch_signals(signals,step)
        if clear:
            ret.append(0)
        elif clk and not prev_clk:
            if load:
                ret.append(in_data)
            else: ret.append(ret[-1])
        else:
            try:
                ret.append(ret[-1]) 
            except:
                ret.append(0)
        prev_clk = clk
    return ret


